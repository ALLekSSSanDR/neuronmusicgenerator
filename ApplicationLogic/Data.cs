﻿using System.Collections.Generic;

namespace ApplicationLogic
{
    /// <summary>
    /// Класс - хранилище данных для нейронной сети
    /// </summary>
    public class Data
    {
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public Data()
        {
            const int sizeMass = 25;
            NetworkSize = 15;
            Iterations = 25;
            LearningRate = 0.3;
            InputsData = new List<int[,]>();
            OutputData = new int[sizeMass, 2];
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="sizeData">Длина массива с данними мелодии</param>
        public Data(int sizeData)
        {
            NetworkSize = 15;
            Iterations = 25;
            LearningRate = 0.3;
            InputsData = new List<int[,]>();
            OutputData = new int[sizeData, 2];
        }

        /// <summary>
        /// Количество нейронов
        /// </summary>
        public int NetworkSize { get; set; }

        /// <summary>
        /// Количество итераций
        /// </summary>
        public int Iterations { get; set; }

        /// <summary>
        /// Оценка(степень) обучения
        /// </summary>
        public double LearningRate { get; set; }
        
        /// <summary>
        /// Данные входов
        /// </summary>
        public List<int[,]> InputsData { get; set; }

        /// <summary>
        /// Массив выходных данных
        /// </summary>
        public int[,] OutputData { get; set; }
    }
}
