﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ApplicationLogic
{
    /// <summary>
    /// Класс для работы с файлами
    /// </summary>
    public class FileManager
    {

        /// <summary>
        /// Метод открывающий файл с данными
        /// </summary>
        /// <returns>Массив с входными данными</returns>
        public static int[,] OpenFile()
        {
            int[,] mass = null;

            var openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = "c:\\TESSSST",
                Filter = "txt files (*.txt)|*.txt",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if (openFileDialog1.ShowDialog() != DialogResult.OK) return null;

            try
            {
                var myStream = openFileDialog1.OpenFile();
                {
                    var filename = openFileDialog1.FileName;

                    var fileText = File.ReadAllText(filename);
                    
                    mass = ParseStringToMass(fileText);

                    myStream.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
            }
            return mass;
        }

        
        /// <summary>
        /// Метод парсинга строки в массив данных
        /// </summary>
        /// <param name="value">Строка данных</param>
        /// <returns>Массив данных</returns>
        private static int[,] ParseStringToMass(string value)
        {
            var sa = value.Split('\r');

            var outMass = new int[sa.GetLength(0)-1,2];

            for (var i = 0; i < sa.GetLength(0)-1; ++i)
            {
                int j;
                var s = sa[i].Split('\t');
                
                if (int.TryParse(s[0], out j))
                {
                    outMass[i, 0] = j;
                }

                if (int.TryParse(s[1], out j))
                {
                    outMass[i, 1] = j;
                }
            }
            return outMass;
        }

        /// <summary>
        /// Метод сохранения данных в файл
        /// </summary>
        /// <param name="mass">Массив данных</param>
        public static void SaveFile(int[,] mass)
        {
            var saveFileDialog1 = new SaveFileDialog
            {
                InitialDirectory = "c:\\",
                Filter = "txt files (*.txt)|*.txt",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if (saveFileDialog1.ShowDialog() != DialogResult.OK) return;

            try
            {
                var filename = saveFileDialog1.FileName;

                var data = "";
                for (var i = 0; i < mass.GetLength(0) - 1; i++)
                {
                    data += mass[i, 0].ToString() + '\t' + mass[i, 1].ToString() + '\r' + '\n';
                }

                File.WriteAllText(filename, data);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
            }
        }

        /// <summary>
        /// Перобразование списка массивов в массив
        /// </summary>
        /// <returns></returns>
        public static int[,] ListToMass(List<int[,]> list)
        {
            var n = list.Select(item => item.GetLength(0)).Concat(new[] {0}).Max();

            var result = new int[n,2];

            for (var i = 0; i < list.Count; i++)
            {
                if (i==0)
                {
                    for (var j = 0; j < list[i].GetLength(0); j++)
                    {
                        result[j, 0] = list[i][j, 0];
                        result[j, 1] = list[i][j, 1];
                    }
                }
                else
                {
                    for (var j = 0; j < list[i].GetLength(0); j++)
                    {
                        result[j, 0] = (result[j, 0] + list[i][j, 0])/2;
                        result[j, 1] = (result[j, 1] + list[i][j, 1])/2;
                    }
                }
            }

            return result;
        }
    }
}
