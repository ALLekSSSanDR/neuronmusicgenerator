﻿using System;

namespace ApplicationLogic
{
    /// <summary>
    /// Класс содержащий логику с мелодией
    /// </summary>
    public class MusicManager
    {
        /// <summary>
        /// Генерация массива со случайными частотами нот и длительностями
        /// </summary>
        /// <param name="sizeMass">Длина массива</param>
        /// <returns>Массив данных</returns>
        public static int[,] GenerateRandomNote(int sizeMass)
        {
            var mass = new int[sizeMass,2];
            var rand = new Random();

            for (var i = 0; i < sizeMass; i++)
            {
                mass[i, 0] = GetDuration(rand.Next(1, 5));
                mass[i, 1] = rand.Next(220, 880);
            }
            return mass;
        }

        /// <summary>
        /// Метод генерации базовой мелодии
        /// </summary>
        /// <returns>Массив с данными</returns>
        public static int[,] BasicNote1()
        {
            //Ноты
            int[] mary =
            {
                330, 294, 262, 294, 330, 
                330, 330, 294, 294, 294,
                330, 392, 392, 330, 294,
                262, 294, 330, 330, 330, 
                330, 294, 294, 330, 294
            };

            //Длительности
            int[] del =
            {
                500, 500, 500, 500, 500, 
                500, 1000,500, 500, 1000,
                500, 500, 100, 500, 500, 
                500, 500, 500, 500, 500, 
                500, 500, 500, 500, 500
            };

            var mass = new int[mary.GetLength(0), 2];

            for (var i = 0; i < mary.Length; i++)
            {
                mass[i, 0] = del[i];
                mass[i, 1] = mary[i];
            }
            return mass;
        }

        /// <summary>
        /// Метод генерации базовой мелодии
        /// </summary>
        /// <returns>Массив с данными</returns>
        public static int[,] BasicNote2()
        {
            //Ноты
            int[] mary =
            {
                530, 494, 462, 494, 530, 
                530, 530, 494, 494, 494,
                530, 592, 592, 530, 494,
                462, 494, 530, 530, 530, 
                530, 494, 494, 530, 494
            };

            //Длительности
            int[] del =
            {
                500, 500, 500, 500, 500, 
                500, 1000,500, 500, 1000,
                500, 500, 100, 500, 500, 
                500, 500, 500, 500, 500, 
                500, 500, 500, 500, 500
            };

            var mass = new int[mary.GetLength(0), 2];

            for (var i = 0; i < mary.Length; i++)
            {
                mass[i, 0] = del[i];
                mass[i, 1] = mary[i];
            }
            return mass;
        }

        /// <summary>
        /// Метод генерации базовой мелодии
        /// </summary>
        /// <returns>Массив с данными</returns>
        public static int[,] BasicNote3()
        {
            //Ноты
            int[] mary =
            {
                730, 694, 662, 694, 730, 
                730, 730, 694, 694, 694,
                730, 792, 792, 730, 694,
                662, 694, 730, 730, 730, 
                730, 694, 694, 730, 694
            };

            //Длительности
            int[] del =
            {
                500, 500, 500, 500, 500, 
                500, 1000,500, 500, 1000,
                500, 500, 100, 500, 500, 
                500, 500, 500, 500, 500, 
                500, 500, 500, 500, 500
            };

            var mass = new int[mary.GetLength(0), 2];

            for (var i = 0; i < mary.Length; i++)
            {
                mass[i, 0] = del[i];
                mass[i, 1] = mary[i];
            }
            return mass;
        }

        /// <summary>
        /// Метод возвращающий длительность ноты в единицах миллисекунд.
        /// </summary>
        /// <param name="intex">Число от 0 до 5</param>
        /// <returns>Длительность</returns>
        public static int GetDuration(int intex)
        {
            switch (intex)
            {
                case 0:
                    return 1600;
                case 1:
                    return 800;
                case 2:
                    return 400;
                case 3:
                    return 200;
                case 4:
                    return 100;
                default:
                    return 5;
            }
        }

        /// <summary>
        /// Воспроизведение массива частот
        /// </summary>
        /// <param name="data">Массив с длительностями и нотами</param>
        public static void PlayNotes(int[,] data)
        {
            for (var i = 0; i < data.GetLength(0); i++)
            {
                try
                {
                    Console.Beep(data[i, 1], data[i, 0]);
                }
                catch (Exception)
                {
                    Console.WriteLine("Значение частоты вышло за допустимый диапазон!");
                }

            }
        }
    }
}
