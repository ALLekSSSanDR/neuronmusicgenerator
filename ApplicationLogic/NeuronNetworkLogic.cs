﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using AForge.Neuro;
using AForge.Neuro.Learning;
using AForge;

namespace ApplicationLogic
{
    /// <summary>
    /// Класс с логикой нейронной сети
    /// </summary>
    public class NeuronNetworkLogic
    {
        /// <summary>
        /// Параметры для приложения
        /// </summary>
        public Data DataNetWork;

        /// <summary>
        /// Конструктор
        /// </summary>
        public NeuronNetworkLogic()
        {
            DataNetWork = new Data();
        }
        
        /// <summary>
        /// Метод - старт для генерации мелодии
        /// </summary>
        /// <param name="listInputData">Список массивов входных данных</param>
        /// <returns>Массив выходных данных</returns>
        public int[,] Start(List<int[,]> listInputData )
        {
            var outList = new List<int[,]>();

            foreach (var massItem in listInputData)
            {


                var note = new int[massItem.GetLength(0), 2];
                var dur = new int[massItem.GetLength(0), 2];

                for (var i = 0; i < massItem.GetLength(0); i++)
                {
                    note[i, 0] = i;
                    dur[i, 0] = i;

                    note[i, 1] = massItem[i, 1];
                    dur[i, 1] = massItem[i, 0];
                }

                var note2 = SearchSolution(note);
                var dur2 = SearchSolution(dur);

                var outMass = new int[note2.GetLength(0), 2];
                for (var i = 0; i < outMass.GetLength(0); i++)
                {
                    outMass[i, 0] = dur2[i, 1];
                    outMass[i, 1] = note2[i, 1];
                }
                outList.Add(outMass);
            }
            return FileManager.ListToMass(outList);
        }
        
        /// <summary>
        /// Метод поиска решения
        /// </summary>
        /// <param name="mass">Массив входных данных</param>
        /// <returns>Массив сгенерированных данных</returns>
        public int[,] SearchSolution(int[,] mass)
        {
            int[,] outputMass;
            var iterations = 0;

            do
            {
                var trainingSet = new double[mass.GetLength(0)][];

                for (var i = 0; i < mass.GetLength(0); i++)
                    trainingSet[i] = new double[] { mass[i, 0], mass[i, 1] };
                
                // Установление диапазона случайных чисел
                Neuron.RandRange = new Range(0, Math.Max(220, 880));

                // Создание сети
                var network = new DistanceNetwork(2, DataNetWork.NetworkSize * DataNetWork.NetworkSize);

                // Создание обучающего алгоритма
                var trainer = new SOMLearning(network, DataNetWork.NetworkSize, DataNetWork.NetworkSize);

                // Создание выходных данных
                var mapSolution = new int[DataNetWork.NetworkSize, DataNetWork.NetworkSize, 3];

                var fixedLearningRate = DataNetWork.LearningRate / 10;
                var driftingLearningRate = fixedLearningRate * 9;

                // Счетчик итераций
                var iteration = 0;

                while (iteration < DataNetWork.Iterations)
                {
                    trainer.LearningRate = driftingLearningRate * (DataNetWork.Iterations - iteration) / DataNetWork.Iterations + fixedLearningRate;
                    trainer.LearningRadius = 3.0 * (DataNetWork.Iterations - iteration) / DataNetWork.Iterations;

                    // Запуск обучающей эпохи
                    trainer.RunEpoch(trainingSet);

                    // Обновление карты
                    UpdateMap(network, trainingSet, mapSolution);

                    // Увеличиваем текущую итерацию
                    iteration++;
                }
                outputMass = GetOutputData(mapSolution,mass);
                iterations++;

                if (IsFinishMass(outputMass))
                    return outputMass;
            } 
            while (iterations >= 500);
            return outputMass;
        }

        /// <summary>
        /// Метод обновления карты сети
        /// </summary>
        /// <param name="network">Нейронная сеть</param>
        /// <param name="trainingSet">Тренеровочных сет</param>
        /// <param name="mapSolution">Карта решений</param>
        private void UpdateMap(DistanceNetwork network, double[][] trainingSet, int[, ,] mapSolution)
        {
            var layer = network.Layers[0];

            Monitor.Enter(this);

            // Проход по всем нейронам 
            for (var i = 0; i < layer.Neurons.Length; i++)
            {
                var neuron = layer.Neurons[i];

                var x = i % DataNetWork.NetworkSize;
                var y = i / DataNetWork.NetworkSize;

                mapSolution[y, x, 0] = (int)neuron.Weights[0];
                mapSolution[y, x, 1] = (int)neuron.Weights[1];
                mapSolution[y, x, 2] = 0;
            }

            // Получить активные нейроны
            for (var i = 0; i < trainingSet.GetLength(0); i++)
            {
                network.Compute(trainingSet[i]);
                var w = network.GetWinner();
                mapSolution[w / DataNetWork.NetworkSize, w % DataNetWork.NetworkSize, 2] = 1;
            }
        }

        /// <summary>
        /// Метод получения выходных данных из карты решения
        /// </summary>
        /// <param name="mapSolution">Карта решений</param>
        /// <returns>Массив выходных данных</returns>
        public int[,] GetOutputData(int[, ,] mapSolution, int[,] mass)
        {
            var result = new int[mass.GetLength(0), 2];

            //Получением из карты решения массива с выходнымим данныи
            var t = 0;
            for (var j = 0; j < DataNetWork.NetworkSize; j++)
            {
                for (var k = 0; k < DataNetWork.NetworkSize; k++)
                {
                    if (mapSolution[j, k, 2] != 1) continue;
                    result[t, 0] = mapSolution[j, k, 0];
                    result[t, 1] = mapSolution[j, k, 1];
                    t++;
                }
            }
            SortMass(result);
            return result;
        }

        /// <summary>
        /// Метод сортировки массива по возрастанию
        /// </summary>
        /// <param name="mass">Массив</param>
        private static void SortMass(int[,] mass)
        {
            for (var i = 0; i < mass.GetLength(0) - 1; i++)
            {
                for (var j = i; j < mass.GetLength(0); j++)
                {
                    if (mass[i, 0] <= mass[j, 0]) continue;

                    var x = mass[i, 0];
                    var y = mass[i, 1];

                    mass[i, 0] = mass[j, 0];
                    mass[i, 1] = mass[j, 1];

                    mass[j, 0] = x;
                    mass[j, 1] = y;
                }
            }
        }

        /// <summary>
        /// Метод определяющий окончание поиска решения
        /// </summary>
        /// <param name="mass">Массив данных</param>
        /// <returns>Истина или ложь</returns>
        public static bool IsFinishMass(int[,] mass)
        {
            var flag = true;
            var check = 0;
            for (var i = 0; i < mass.GetLength(0); i++)
            {
                if (mass[i, 1] != 0) continue;
                flag = false;
                check++;
            }
            return check <= 1 || flag;
        }


        /// <summary>
        /// Перобразование списка массивов в массив
        /// </summary>
        /// <returns></returns>
        public static int[,] ListToLongMass(List<int[,]> list)
        {
            var n = list.Sum(item => item.GetLength(0));

            var result = new int[n, 2];

            var k = 0;

            foreach (var t in list)
            {
                for (var j = 0; j < t.GetLength(0); j++)
                {
                    result[k, 0] = t[j, 0];
                    result[k, 1] = t[j, 1];
                    k++;
                }
            }
            return result;
        }
    }
}
