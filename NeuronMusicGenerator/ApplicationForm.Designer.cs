﻿using System;
using System.Drawing;
using System.Threading;
using AForge;
using AForge.Neuro;
using AForge.Neuro.Learning;

using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace NeuronMusicGenerator
{
    partial class ApplicationForm : System.Windows.Forms.Form
    {
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox rateBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox iterationsBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox sizeBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel pointsPanel;
        private System.Windows.Forms.Panel mapPanel;

        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.startButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.rateBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.iterationsBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.sizeBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_SaveOutputData = new System.Windows.Forms.Button();
            this.button_PlayOutputData = new System.Windows.Forms.Button();
            this.mapPanel = new System.Windows.Forms.Panel();
            this.generateButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_basicMelody = new System.Windows.Forms.Button();
            this.button_LoadInputData = new System.Windows.Forms.Button();
            this.button_PlayInputData = new System.Windows.Forms.Button();
            this.pointsPanel = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.sizeMassBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(10, 254);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(200, 52);
            this.startButton.TabIndex = 15;
            this.startButton.Text = "Старт";
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Location = new System.Drawing.Point(13, 239);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(160, 2);
            this.label7.TabIndex = 12;
            // 
            // rateBox
            // 
            this.rateBox.Location = new System.Drawing.Point(13, 198);
            this.rateBox.Name = "rateBox";
            this.rateBox.Size = new System.Drawing.Size(157, 20);
            this.rateBox.TabIndex = 9;
            this.rateBox.Text = "3";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(13, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(160, 30);
            this.label5.TabIndex = 8;
            this.label5.Text = "Скорость генерации (может влиять на длину массива):";
            // 
            // iterationsBox
            // 
            this.iterationsBox.Location = new System.Drawing.Point(13, 130);
            this.iterationsBox.Name = "iterationsBox";
            this.iterationsBox.Size = new System.Drawing.Size(157, 20);
            this.iterationsBox.TabIndex = 7;
            this.iterationsBox.Text = "500";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(13, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(160, 29);
            this.label6.TabIndex = 6;
            this.label6.Text = "Точность соответствие стиля:";
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(13, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 2);
            this.label3.TabIndex = 3;
            // 
            // sizeBox
            // 
            this.sizeBox.Location = new System.Drawing.Point(13, 51);
            this.sizeBox.Name = "sizeBox";
            this.sizeBox.Size = new System.Drawing.Size(157, 20);
            this.sizeBox.TabIndex = 1;
            this.sizeBox.Text = "15";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(10, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Длина генерируемых данных (количество нейронов:)";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.startButton);
            this.groupBox2.Controls.Add(this.button_SaveOutputData);
            this.groupBox2.Controls.Add(this.button_PlayOutputData);
            this.groupBox2.Controls.Add(this.mapPanel);
            this.groupBox2.Location = new System.Drawing.Point(246, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(220, 322);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Сгенерированные данные";
            // 
            // button_SaveOutputData
            // 
            this.button_SaveOutputData.Location = new System.Drawing.Point(116, 225);
            this.button_SaveOutputData.Name = "button_SaveOutputData";
            this.button_SaveOutputData.Size = new System.Drawing.Size(94, 23);
            this.button_SaveOutputData.TabIndex = 6;
            this.button_SaveOutputData.Text = "Сохранить";
            this.button_SaveOutputData.UseVisualStyleBackColor = true;
            this.button_SaveOutputData.Click += new System.EventHandler(this.button_SaveOutputData_Click);
            // 
            // button_PlayOutputData
            // 
            this.button_PlayOutputData.Location = new System.Drawing.Point(10, 225);
            this.button_PlayOutputData.Name = "button_PlayOutputData";
            this.button_PlayOutputData.Size = new System.Drawing.Size(100, 23);
            this.button_PlayOutputData.TabIndex = 5;
            this.button_PlayOutputData.Text = "Воспроизвести";
            this.button_PlayOutputData.UseVisualStyleBackColor = true;
            this.button_PlayOutputData.Click += new System.EventHandler(this.button_PlayOutputData_Click);
            // 
            // mapPanel
            // 
            this.mapPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mapPanel.Location = new System.Drawing.Point(10, 18);
            this.mapPanel.Name = "mapPanel";
            this.mapPanel.Size = new System.Drawing.Size(200, 200);
            this.mapPanel.TabIndex = 4;
            this.mapPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.mapPanel_Paint);
            // 
            // generateButton
            // 
            this.generateButton.Location = new System.Drawing.Point(10, 254);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(200, 23);
            this.generateButton.TabIndex = 1;
            this.generateButton.Text = "Генерировать";
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_basicMelody);
            this.groupBox1.Controls.Add(this.button_LoadInputData);
            this.groupBox1.Controls.Add(this.button_PlayInputData);
            this.groupBox1.Controls.Add(this.pointsPanel);
            this.groupBox1.Controls.Add(this.generateButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(220, 322);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Входные данные";
            // 
            // button_basicMelody
            // 
            this.button_basicMelody.Location = new System.Drawing.Point(10, 282);
            this.button_basicMelody.Name = "button_basicMelody";
            this.button_basicMelody.Size = new System.Drawing.Size(200, 23);
            this.button_basicMelody.TabIndex = 6;
            this.button_basicMelody.Text = "Базовая мелодия";
            this.button_basicMelody.UseVisualStyleBackColor = true;
            this.button_basicMelody.Click += new System.EventHandler(this.button_basicMelody_Click);
            // 
            // button_LoadInputData
            // 
            this.button_LoadInputData.Location = new System.Drawing.Point(116, 225);
            this.button_LoadInputData.Name = "button_LoadInputData";
            this.button_LoadInputData.Size = new System.Drawing.Size(94, 23);
            this.button_LoadInputData.TabIndex = 5;
            this.button_LoadInputData.Text = "Загрузить";
            this.button_LoadInputData.UseVisualStyleBackColor = true;
            this.button_LoadInputData.Click += new System.EventHandler(this.button_LoadInputData_Click);
            // 
            // button_PlayInputData
            // 
            this.button_PlayInputData.Location = new System.Drawing.Point(10, 225);
            this.button_PlayInputData.Name = "button_PlayInputData";
            this.button_PlayInputData.Size = new System.Drawing.Size(100, 23);
            this.button_PlayInputData.TabIndex = 4;
            this.button_PlayInputData.Text = "Воспроизвести";
            this.button_PlayInputData.UseVisualStyleBackColor = true;
            this.button_PlayInputData.Click += new System.EventHandler(this.button_PlayInputData_Click);
            // 
            // pointsPanel
            // 
            this.pointsPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pointsPanel.Location = new System.Drawing.Point(10, 18);
            this.pointsPanel.Name = "pointsPanel";
            this.pointsPanel.Size = new System.Drawing.Size(200, 200);
            this.pointsPanel.TabIndex = 0;
            this.pointsPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.pointsPanel_Paint);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.sizeMassBox);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.rateBox);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.iterationsBox);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.sizeBox);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(480, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(182, 322);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Параметры нейронной сети";
            // 
            // sizeMassBox
            // 
            this.sizeMassBox.Location = new System.Drawing.Point(13, 282);
            this.sizeMassBox.Name = "sizeMassBox";
            this.sizeMassBox.Size = new System.Drawing.Size(157, 20);
            this.sizeMassBox.TabIndex = 14;
            this.sizeMassBox.Text = "25";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(13, 247);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 36);
            this.label2.TabIndex = 13;
            this.label2.Text = "Длина генерируемого массива:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ApplicationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 342);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ApplicationForm";
            this.Text = "Application";
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Button button_PlayOutputData;
        private Button button_PlayInputData;
        private Button button_SaveOutputData;
        private Button button_LoadInputData;
        private TextBox sizeMassBox;
        private Label label2;
        private Button button_basicMelody;


    }
}

