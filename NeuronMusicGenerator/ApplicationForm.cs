﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
    
using ApplicationLogic;

namespace NeuronMusicGenerator
{
    /// <summary>
    /// GUI
    /// </summary>
    public partial class ApplicationForm
    {
        /// <summary>
        /// Обьект класса с нейронной сетью
        /// </summary>
        private readonly NeuronNetworkLogic _neuronNetwork;
        
        /// <summary>
        /// Конструктор
        /// </summary>
        public ApplicationForm()
        {
            InitializeComponent();
            var listInputs = new List<int[,]>();
            listInputs.Add(MusicManager.BasicNote1()); 
            _neuronNetwork = new NeuronNetworkLogic {DataNetWork = {InputsData = listInputs}};
            UpdateSettings();
            pointsPanel.Invalidate();
        }

        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new ApplicationForm());
        }
 
        /// <summary>
        /// Обновление полей с параметрами 
        /// </summary>
        private void UpdateSettings()
        {
            sizeBox.Text = _neuronNetwork.DataNetWork.NetworkSize.ToString();       
            iterationsBox.Text = _neuronNetwork.DataNetWork.Iterations.ToString();   
            rateBox.Text = _neuronNetwork.DataNetWork.LearningRate.ToString();     
        }

        /// <summary>
        /// Обработчик кнопки генерации входных данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void generateButton_Click(object sender, EventArgs e)
        {
            try
            {
                _neuronNetwork.DataNetWork = new Data(Int32.Parse(sizeMassBox.Text));
            }
            catch (Exception)
            {
                _neuronNetwork.DataNetWork = new Data();
            }
            _neuronNetwork.DataNetWork.InputsData.Add(MusicManager.GenerateRandomNote(_neuronNetwork.DataNetWork.OutputData.GetLength(0)));
            pointsPanel.Invalidate();
            UpdateSettings();
        }
        
        /// <summary>
        /// Рисование сгенерированных точек
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pointsPanel_Paint(object sender, PaintEventArgs e)
        {
            Graphics.DrawGraphic(e, NeuronNetworkLogic.ListToLongMass(_neuronNetwork.DataNetWork.InputsData));
        }

       
        /// <summary>
        /// Обработчик рисования полученной карты
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mapPanel_Paint(object sender, PaintEventArgs e)
        {
            Graphics.DrawGraphic(e,_neuronNetwork.DataNetWork.OutputData);
        }
   
    
        /// <summary>
        /// Обработчик кнопки СТАРТ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startButton_Click(object sender, EventArgs e)
        {
            try
            {
                _neuronNetwork.DataNetWork.NetworkSize = Math.Max(5, Math.Min(50, int.Parse(sizeBox.Text)));

            }
            catch (Exception)
            {
                _neuronNetwork.DataNetWork.NetworkSize = 25;
            }

            try
            {
                _neuronNetwork.DataNetWork.Iterations= Math.Max(10, Math.Min(100000, int.Parse(iterationsBox.Text)));
            }
            catch (Exception)
            {
                _neuronNetwork.DataNetWork.Iterations = 25;
            }

            try
            {
                _neuronNetwork.DataNetWork.LearningRate= Math.Max(0.01, Math.Min(50.0d, double.Parse(rateBox.Text)));
            }
            catch (Exception)
            {
                _neuronNetwork.DataNetWork.LearningRate = 0.3;
            }
             
            
            UpdateSettings();

            _neuronNetwork.DataNetWork.OutputData = _neuronNetwork.Start(_neuronNetwork.DataNetWork.InputsData);

            //Нагузочное тестирование
//            Test();

            mapPanel.Invalidate();
            pointsPanel.Invalidate();
        }

        /// <summary>
        /// Кнопка воспроизведения входной мелодии
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_PlayInputData_Click(object sender, EventArgs e)
        {
            foreach (var mass in _neuronNetwork.DataNetWork.InputsData)
            {
                MusicManager.PlayNotes(mass);
            }
        }
       
        /// <summary>
        /// Кнопка воспроизведения выходной мелодии
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_PlayOutputData_Click(object sender, EventArgs e)
        {
            MusicManager.PlayNotes(_neuronNetwork.DataNetWork.OutputData);
        }

        /// <summary>
        /// Кнопка загрузки входных данных из файла
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_LoadInputData_Click(object sender, EventArgs e)
        {
            var list = new List<int[,]>() { MusicManager.BasicNote1(), MusicManager.BasicNote2(), MusicManager.BasicNote3() };

            var formLoadData = new LoadDataForm(list) { Owner = this };

            formLoadData.ShowDialog();

            var data = formLoadData.OutputListData;

            _neuronNetwork.DataNetWork.InputsData = data;

            pointsPanel.Invalidate();
        }

        /// <summary>
        /// Кнопка сохранения выходных данных в файл
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_SaveOutputData_Click(object sender, EventArgs e)
        {
            FileManager.SaveFile(_neuronNetwork.DataNetWork.OutputData);
        }
        
        /// <summary>
        /// Кнопка генерации базовой мелодии 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_basicMelody_Click(object sender, EventArgs e)
        {
            try
            {
                var data = new List<int[,]>{MusicManager.BasicNote1()};
                _neuronNetwork.DataNetWork = new Data() { InputsData = data };
            }
            catch (Exception)
            {
                _neuronNetwork.DataNetWork = new Data();
            }
            pointsPanel.Invalidate();
        }

        /// <summary>
        /// Метод для нагрузочного тестирования
        /// </summary>
        private static void Test()
        {
#if DEBUG
            // Диагностика времени выполнения функции
            System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();

            // Создание и открытие его для записи
            var file = new StreamWriter("C:\\TESSSST/Test1.txt");

            for (var i = 0; i < 100; i++)
            {
                // Сброс
                myStopwatch.Reset();
                // Запуск
                myStopwatch.Start();

                var listInputs = new List<int[,]>()
                {
                    MusicManager.BasicNote1(),
                    //MusicManager.BasicNote2(),
                    //MusicManager.BasicNote3()
                };
                var neuronNetwork = new NeuronNetworkLogic { DataNetWork = { InputsData = listInputs } };

                neuronNetwork.Start(neuronNetwork.DataNetWork.InputsData);

                // Остановить
                myStopwatch.Stop();

                var ts = myStopwatch.Elapsed;
                var elapsedTime = String.Format("{0:f}", ts.Milliseconds);
                // Запись в файл результатов
                file.Write(elapsedTime + "\n");
            }
            file.Close();
        }
#endif
    }
}

