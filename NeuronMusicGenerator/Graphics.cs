﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace NeuronMusicGenerator
{
    public class Graphics
    {
        /// <summary>
        /// Метод отрисовки точек массива на панели
        /// </summary>
        /// <param name="e">Графический элемент панель</param>
        /// <param name="data">Данные массива</param>
        public static void DrawGraphic(PaintEventArgs e, int[,] data)
        {
            var graphics = e.Graphics;

            var listY = new List<int>();

            for (var i = 0; i < data.GetLength(0); i++)
                listY.Add(data[i, 1]);

            var scaleX = 0.0f;
            var scaleY = 0.0f;

            try
            {
                scaleX = 200f/data.GetLength(0);
                scaleY = 200f/(listY.Max());
            }
            catch (Exception)
            {
                // ignored
            }


            using (Brush brush = new SolidBrush(Color.Blue))
            {
                var x = 0f;
                for (int i = 0, n = data.GetLength(0); i < n; i++)
                {
                    var y = (data[i, 1] - 2)*scaleY;
                    graphics.FillEllipse(brush, x, y, 5, 5);
                    x += scaleX;
                }
            }
        }

        /// <summary>
        /// Метод заполняющий ListBox данными массива
        /// </summary>
        /// <param name="listBox">Графический элемент список</param>
        /// <param name="data">Данные массива</param>
        public static void SetList(ListBox listBox, int[,] data)
        {
            listBox.Items.Clear();
            for (var j = 0; j < data.GetLength(0); j++)
                listBox.Items.Add(j + "\t" + data[j, 0] + "\t" + data[j, 1]);
        }
    }
}
