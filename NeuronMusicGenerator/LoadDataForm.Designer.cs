﻿using System.Windows.Forms;

namespace NeuronMusicGenerator
{
    partial class LoadDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOk = new System.Windows.Forms.Button();
            this.listViewLoadData = new System.Windows.Forms.ListView();
            this.buttonAddDataItem = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(172, 220);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(100, 23);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "Ок";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // listViewLoadData
            // 
            this.listViewLoadData.CheckBoxes = true;
            this.listViewLoadData.FullRowSelect = true;
            this.listViewLoadData.GridLines = true;
            this.listViewLoadData.Location = new System.Drawing.Point(12, 12);
            this.listViewLoadData.Name = "listViewLoadData";
            this.listViewLoadData.Size = new System.Drawing.Size(260, 200);
            this.listViewLoadData.TabIndex = 4;
            this.listViewLoadData.UseCompatibleStateImageBehavior = false;
            this.listViewLoadData.View = System.Windows.Forms.View.Details;
            this.listViewLoadData.Columns.Add("", 200);

            // 
            // buttonAddDataItem
            // 
            this.buttonAddDataItem.Location = new System.Drawing.Point(12, 220);
            this.buttonAddDataItem.Name = "buttonAddDataItem";
            this.buttonAddDataItem.Size = new System.Drawing.Size(100, 22);
            this.buttonAddDataItem.TabIndex = 5;
            this.buttonAddDataItem.Text = "Добавить ";
            this.buttonAddDataItem.UseVisualStyleBackColor = true;
            this.buttonAddDataItem.Click += new System.EventHandler(this.buttonAddDataItem_Click);
            // 
            // LoadDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 252);
            this.Controls.Add(this.buttonAddDataItem);
            this.Controls.Add(this.listViewLoadData);
            this.Controls.Add(this.buttonOk);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoadDataForm";
            this.Text = "LoadDataForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.ListView listViewLoadData;
        private System.Windows.Forms.Button buttonAddDataItem;
    }
}