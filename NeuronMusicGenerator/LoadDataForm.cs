﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ApplicationLogic;

namespace NeuronMusicGenerator
{
    public partial class LoadDataForm : Form
    {
        /// <summary>
        /// Временное хранилище загруженных данных
        /// </summary>
        private readonly List<int[,]> _listData;

        /// <summary>
        /// Выбранные данные
        /// </summary>
        public List<int[,]> OutputListData { get; set; } 

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public LoadDataForm()
        {
            _listData = new List<int[,]>();
            InitializeComponent();
        }

        /// <summary>
        /// Конструктор с входными данными
        /// </summary>
        /// <param name="listData">Список массивов с данными</param>
        public LoadDataForm(List<int[,]> listData)
        {
            _listData = listData;
            InitializeComponent();

            for (var i = 0; i < listData.Count; i++)
            {
                listViewLoadData.Items.Add(new ListViewItem()
                {
                    Text = "Мелодия " + i,
                    Checked = true
                });
            }
        }

        /// <summary>
        /// Обработчик кнопки Ок
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOk_Click(object sender, EventArgs e)
        {
            OutputListData = new List<int[,]>();
            for (var i = 0; i < listViewLoadData.Items.Count; i++)
            {
                if (listViewLoadData.Items[i].Checked)
                {
                    OutputListData.Add(_listData[i]);
                }
            }
            Close();
        }

        /// <summary>
        /// Обработчик кнопки добавить
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddDataItem_Click(object sender, EventArgs e)
        {
            var dataFile = FileManager.OpenFile();

            _listData.Add(dataFile);

            listViewLoadData.Items.Add(new ListViewItem()
            {
                Text = "Мелодия " + listViewLoadData.Items.Count,
                Checked = true
            });
        }
    }
}
